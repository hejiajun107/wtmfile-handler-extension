# WTMFileHandlerExtension

#### 介绍
支持WTM框架的扩展，因官方目前只有AliOSS,所以提供额外的选项，目前支持腾讯云COS，计划支持更多


#### 安装教程
```
dotnet add package WTMFileHandlerExtensions --version 8.0.0
```

#### 使用说明

##### appsetting.json

```
"FileUploadOptions": {
  ... 
  "SaveFileMode": "COS", //tecentcloud cos,huaweicloud obs
  ...
}

  "WtmTencentCosConfig": {
    "BucketName": "",
    "AppId": "",
    "Region": "",
    "SecretId": "",
    "SecretKey": "",
    "GroupName": ""
  },

  "WtmHuaweiObsConfig": {
    "AccessKeyID": "",
    "SecretAccessKey": "",
    "EndPoint": "",
    "BucketName": "",
    "GroupName": "",
  },
```

##### startup.js

```
using WTMFileHandlerExtensions;

public void ConfigureServices(IServiceCollection services)
{
        ...
               services.AddWtmFileHandlerExt(ConfigRoot);
        ...
}
```


