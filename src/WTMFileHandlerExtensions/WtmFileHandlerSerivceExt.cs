﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WTMFileHandlerExtensions.Huawei;
using WTMFileHandlerExtensions.Tencent;

namespace WTMFileHandlerExtensions
{
    public static class WtmFileHandlerSerivceExt
    {
        public static IServiceCollection AddWtmFileHandlerExt(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<TencentCosConfig>(config.GetSection("WtmTencentCosConfig"));
            services.Configure<HuaweiObsConfig>(config.GetSection("WtmHuaweiObsConfig"));
            return services;
        }
    }
}
