﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTMFileHandlerExtensions.Tencent
{
    public class TencentCosConfig
    {
        /// <summary>
        /// Bucket名
        /// </summary>
        public string BucketName { get; set; }
        /// <summary>
        /// AppId
        /// </summary>
        public string AppId { get; set;}
        /// <summary>
        /// 区域
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// SecretId
        /// </summary>
        public string SecretId { get; set; }
        /// <summary>
        /// SecretKey
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// groupName
        /// </summary>
        public string GroupName { get; set; } = "default";
    }
}
