﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.ComponentModel.DataAnnotations;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Support.FileHandlers;
using OBS;
using WalkingTec.Mvvm.Core.Models;
using GetObjectRequest = OBS.Model.GetObjectRequest;
using DeleteObjectRequest = OBS.Model.DeleteObjectRequest;
using PutObjectRequest = OBS.Model.PutObjectRequest;
using OBS.Model;
using WalkingTec.Mvvm.Core.Extensions;
using System.IO;

namespace WTMFileHandlerExtensions.Huawei
{
    [Display(Name = "obs")]
    public class WtmObsFileHandler : WtmFileHandlerBase
    {
        public WtmObsFileHandler(WTMContext wtm) : base(wtm)
        {
            _config = wtm.ServiceProvider.GetRequiredService<IOptions<HuaweiObsConfig>>().Value;
        }

        private HuaweiObsConfig _config;
        private ObsClient _client;

        private ObsClient CreateObsClient()
        {
            if(_client is not null)
                return _client;

            _client = new ObsClient(_config.AccessKeyID, _config.SecretAccessKey, "https://your-endpoint");
        
            return _client;
        }



        public override Stream GetFileData(IWtmFile file)
        {
            var client = CreateObsClient();
            GetObjectRequest request = new GetObjectRequest()
            {
                BucketName = "bucketname",
                ObjectKey = file.Path,
            };
            GetObjectResponse response = client.GetObject(request);
            response.OutputStream.Seek(0, SeekOrigin.Begin);
            return response.OutputStream;
        }


        public override (string path, string handlerInfo) Upload(string fileName, long fileLength, Stream data, string group = null, string subdir = null, string extra = null)
        {
            var ext = string.Empty;
            if (string.IsNullOrEmpty(fileName) == false)
            {
                var dotPos = fileName.LastIndexOf('.');
                ext = fileName.Substring(dotPos + 1);
            }

            string pathHeader = "";
            if (string.IsNullOrEmpty(subdir) == false)
            {
                pathHeader = Path.Combine(pathHeader, subdir);
            }
            else
            {
                var sub = WtmFileProvider._subDirFunc?.Invoke(this);
                if (string.IsNullOrEmpty(sub) == false)
                {
                    pathHeader = Path.Combine(pathHeader, sub);
                }
            }
            var fullPath = Path.Combine(pathHeader, $"{Guid.NewGuid().ToNoSplitString()}.{ext}");
            fullPath = fullPath.Replace("\\", "/");

            PutObjectRequest request = new PutObjectRequest()
            {
                BucketName = _config.BucketName,
                ObjectKey = fullPath,
                InputStream = data,
            };
            var client = CreateObsClient();
            PutObjectResponse response = client.PutObject(request);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return (fullPath, _config.GroupName);
            }
            else
            {
                return (null, null);
            }
        }

        public override void DeleteFile(IWtmFile file)
        {
            try
            {
                var client = CreateObsClient();
                DeleteObjectRequest request = new DeleteObjectRequest()
                {
                    BucketName = _config.BucketName,
                    ObjectKey = file.Path,
                };
                client.DeleteObject(request);
            }
            catch { }
            return;
        }

    }
}
