﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTMFileHandlerExtensions.Huawei
{
    public class HuaweiObsConfig
    {
        public string AccessKeyID { get; set; }
        public string SecretAccessKey { get; set; }

        public string EndPoint { get; set; }

        public string BucketName { get; set; }

        public string GroupName { get; set; }
    }

}
